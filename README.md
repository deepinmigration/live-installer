# Live Installer
livecd 安装器

## 已完成
* 系统基本可安装
* 神威平台自动生成需要的`/boot`目录
* 自动通过`/etc/lsb_release`生成启动菜单名称

## 自定义

## 配置

使用`/etc/live-installer/install.conf`配置

```
# Defines the distribution name and version..
[distribution]
DISTRIBUTION_NAME=ubuntu
DISTRIBUTION_VERSION=1.0
DISTRIBUTION_DESCRIPTION=Ubuntu Unity 16.04
DISTRIBUTION_LOGO=/usr/share/live-installer/logos/ubuntu.svg
# Installation specific paths etc.
[install]
LIVE_MEDIA_SOURCE = /cdrom/casper/filesystem.squashfs
LIVE_MEDIA_TYPE = squashfs
# Custom install etc.
[custom]
# BOOTLOADER_TYPES supports grub-embedded grub-native
BOOTLOADER_TYPES = grub-embedded
```

其中 `BOOTLOADER_TYPES` ,目前只实现以下两个方式:
* grub-embedded: 国产昆仑等自带grub作为引导器使用
* grub-native: 执行`grub-install`操作安装到系统

## 自定义
在生成光盘目录下`/live-installer.conf`文件，与上面`/etc/live-install/install.conf`类似，但是安装过程会覆盖文件信息

## TODO
* 自定义hooks功能
* 日志打印存储
* Stand alone 前端启动慢
